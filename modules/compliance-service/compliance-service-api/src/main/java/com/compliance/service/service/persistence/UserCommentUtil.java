/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.persistence;

import com.compliance.service.model.UserComment;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the user comment service. This utility wraps <code>com.compliance.service.service.persistence.impl.UserCommentPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserCommentPersistence
 * @generated
 */
public class UserCommentUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserComment userComment) {
		getPersistence().clearCache(userComment);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, UserComment> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserComment> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserComment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserComment> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserComment update(UserComment userComment) {
		return getPersistence().update(userComment);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserComment update(
		UserComment userComment, ServiceContext serviceContext) {

		return getPersistence().update(userComment, serviceContext);
	}

	/**
	 * Returns all the user comments where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching user comments
	 */
	public static List<UserComment> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public static List<UserComment> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByUuid_First(
			String uuid, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUuid_First(
		String uuid, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByUuid_Last(
			String uuid, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUuid_Last(
		String uuid, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where uuid = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment[] findByUuid_PrevAndNext(
			long commentId, String uuid,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_PrevAndNext(
			commentId, uuid, orderByComparator);
	}

	/**
	 * Removes all the user comments where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of user comments where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching user comments
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchUserCommentException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByUUID_G(String uuid, long groupId)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the user comment where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the user comment where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the user comment that was removed
	 */
	public static UserComment removeByUUID_G(String uuid, long groupId)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of user comments where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching user comments
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching user comments
	 */
	public static List<UserComment> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public static List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment[] findByUuid_C_PrevAndNext(
			long commentId, String uuid, long companyId,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByUuid_C_PrevAndNext(
			commentId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the user comments where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of user comments where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching user comments
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching user comments
	 */
	public static List<UserComment> findByComplianceId(long complianceId) {
		return getPersistence().findByComplianceId(complianceId);
	}

	/**
	 * Returns a range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public static List<UserComment> findByComplianceId(
		long complianceId, int start, int end) {

		return getPersistence().findByComplianceId(complianceId, start, end);
	}

	/**
	 * Returns an ordered range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByComplianceId(
		long complianceId, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findByComplianceId(
			complianceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments where complianceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param complianceId the compliance ID
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByComplianceId(
		long complianceId, int start, int end,
		OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByComplianceId(
			complianceId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByComplianceId_First(
			long complianceId, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByComplianceId_First(
			complianceId, orderByComparator);
	}

	/**
	 * Returns the first user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByComplianceId_First(
		long complianceId, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByComplianceId_First(
			complianceId, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByComplianceId_Last(
			long complianceId, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByComplianceId_Last(
			complianceId, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByComplianceId_Last(
		long complianceId, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByComplianceId_Last(
			complianceId, orderByComparator);
	}

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where complianceId = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param complianceId the compliance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment[] findByComplianceId_PrevAndNext(
			long commentId, long complianceId,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByComplianceId_PrevAndNext(
			commentId, complianceId, orderByComparator);
	}

	/**
	 * Removes all the user comments where complianceId = &#63; from the database.
	 *
	 * @param complianceId the compliance ID
	 */
	public static void removeByComplianceId(long complianceId) {
		getPersistence().removeByComplianceId(complianceId);
	}

	/**
	 * Returns the number of user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the number of matching user comments
	 */
	public static int countByComplianceId(long complianceId) {
		return getPersistence().countByComplianceId(complianceId);
	}

	/**
	 * Returns all the user comments where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching user comments
	 */
	public static List<UserComment> findByStatus(int status) {
		return getPersistence().findByStatus(status);
	}

	/**
	 * Returns a range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public static List<UserComment> findByStatus(
		int status, int start, int end) {

		return getPersistence().findByStatus(status, start, end);
	}

	/**
	 * Returns an ordered range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByStatus(
		int status, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findByStatus(
			status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByStatus(
		int status, int start, int end,
		OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByStatus(
			status, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByStatus_First(
			int status, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByStatus_First(status, orderByComparator);
	}

	/**
	 * Returns the first user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByStatus_First(
		int status, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByStatus_First(status, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByStatus_Last(
			int status, OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByStatus_Last(status, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByStatus_Last(
		int status, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByStatus_Last(status, orderByComparator);
	}

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where status = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment[] findByStatus_PrevAndNext(
			long commentId, int status,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByStatus_PrevAndNext(
			commentId, status, orderByComparator);
	}

	/**
	 * Removes all the user comments where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	public static void removeByStatus(int status) {
		getPersistence().removeByStatus(status);
	}

	/**
	 * Returns the number of user comments where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching user comments
	 */
	public static int countByStatus(int status) {
		return getPersistence().countByStatus(status);
	}

	/**
	 * Returns all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching user comments
	 */
	public static List<UserComment> findByG_S(long groupId, int status) {
		return getPersistence().findByG_S(groupId, status);
	}

	/**
	 * Returns a range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of matching user comments
	 */
	public static List<UserComment> findByG_S(
		long groupId, int status, int start, int end) {

		return getPersistence().findByG_S(groupId, status, start, end);
	}

	/**
	 * Returns an ordered range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findByG_S(
			groupId, status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user comments
	 */
	public static List<UserComment> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByG_S(
			groupId, status, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByG_S_First(
			long groupId, int status,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByG_S_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the first user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByG_S_First(
		long groupId, int status,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByG_S_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment
	 * @throws NoSuchUserCommentException if a matching user comment could not be found
	 */
	public static UserComment findByG_S_Last(
			long groupId, int status,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByG_S_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	public static UserComment fetchByG_S_Last(
		long groupId, int status,
		OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().fetchByG_S_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the user comments before and after the current user comment in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param commentId the primary key of the current user comment
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment[] findByG_S_PrevAndNext(
			long commentId, long groupId, int status,
			OrderByComparator<UserComment> orderByComparator)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByG_S_PrevAndNext(
			commentId, groupId, status, orderByComparator);
	}

	/**
	 * Removes all the user comments where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	public static void removeByG_S(long groupId, int status) {
		getPersistence().removeByG_S(groupId, status);
	}

	/**
	 * Returns the number of user comments where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching user comments
	 */
	public static int countByG_S(long groupId, int status) {
		return getPersistence().countByG_S(groupId, status);
	}

	/**
	 * Caches the user comment in the entity cache if it is enabled.
	 *
	 * @param userComment the user comment
	 */
	public static void cacheResult(UserComment userComment) {
		getPersistence().cacheResult(userComment);
	}

	/**
	 * Caches the user comments in the entity cache if it is enabled.
	 *
	 * @param userComments the user comments
	 */
	public static void cacheResult(List<UserComment> userComments) {
		getPersistence().cacheResult(userComments);
	}

	/**
	 * Creates a new user comment with the primary key. Does not add the user comment to the database.
	 *
	 * @param commentId the primary key for the new user comment
	 * @return the new user comment
	 */
	public static UserComment create(long commentId) {
		return getPersistence().create(commentId);
	}

	/**
	 * Removes the user comment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment that was removed
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment remove(long commentId)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().remove(commentId);
	}

	public static UserComment updateImpl(UserComment userComment) {
		return getPersistence().updateImpl(userComment);
	}

	/**
	 * Returns the user comment with the primary key or throws a <code>NoSuchUserCommentException</code> if it could not be found.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment
	 * @throws NoSuchUserCommentException if a user comment with the primary key could not be found
	 */
	public static UserComment findByPrimaryKey(long commentId)
		throws com.compliance.service.exception.NoSuchUserCommentException {

		return getPersistence().findByPrimaryKey(commentId);
	}

	/**
	 * Returns the user comment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment, or <code>null</code> if a user comment with the primary key could not be found
	 */
	public static UserComment fetchByPrimaryKey(long commentId) {
		return getPersistence().fetchByPrimaryKey(commentId);
	}

	/**
	 * Returns all the user comments.
	 *
	 * @return the user comments
	 */
	public static List<UserComment> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of user comments
	 */
	public static List<UserComment> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user comments
	 */
	public static List<UserComment> findAll(
		int start, int end, OrderByComparator<UserComment> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of user comments
	 */
	public static List<UserComment> findAll(
		int start, int end, OrderByComparator<UserComment> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the user comments from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of user comments.
	 *
	 * @return the number of user comments
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static UserCommentPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<UserCommentPersistence, UserCommentPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(UserCommentPersistence.class);

		ServiceTracker<UserCommentPersistence, UserCommentPersistence>
			serviceTracker =
				new ServiceTracker
					<UserCommentPersistence, UserCommentPersistence>(
						bundle.getBundleContext(), UserCommentPersistence.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}