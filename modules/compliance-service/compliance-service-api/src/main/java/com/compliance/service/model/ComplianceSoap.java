/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.compliance.service.service.http.ComplianceServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ComplianceSoap implements Serializable {

	public static ComplianceSoap toSoapModel(Compliance model) {
		ComplianceSoap soapModel = new ComplianceSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setComplianceId(model.getComplianceId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreatedby(model.getCreatedby());
		soapModel.setModifiedby(model.getModifiedby());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setContractId(model.getContractId());
		soapModel.setEntityId(model.getEntityId());
		soapModel.setApprovalDate(model.getApprovalDate());
		soapModel.setComments(model.getComments());
		soapModel.setApproverComments(model.getApproverComments());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setMiddleName(model.getMiddleName());
		soapModel.setLastName(model.getLastName());
		soapModel.setContractNumber(model.getContractNumber());
		soapModel.setApproverContractNumber(model.getApproverContractNumber());
		soapModel.setApproverVerdict(model.getApproverVerdict());
		soapModel.setApproverAction(model.getApproverAction());
		soapModel.setErrorReason(model.getErrorReason());
		soapModel.setAssignmentStatus(model.getAssignmentStatus());
		soapModel.setCapacity(model.getCapacity());
		soapModel.setNewPositionId1(model.getNewPositionId1());
		soapModel.setNewPositionName1(model.getNewPositionName1());
		soapModel.setNewPositionId2(model.getNewPositionId2());
		soapModel.setNewPositionName2(model.getNewPositionName2());
		soapModel.setNewOrgId1(model.getNewOrgId1());
		soapModel.setNewOrgName1(model.getNewOrgName1());
		soapModel.setNewOrgId2(model.getNewOrgId2());
		soapModel.setNewOrgName2(model.getNewOrgName2());
		soapModel.setHistoryStartDate(model.getHistoryStartDate());
		soapModel.setHistoryEndDate(model.getHistoryEndDate());
		soapModel.setOldOrgId(model.getOldOrgId());
		soapModel.setOldPositionId(model.getOldPositionId());
		soapModel.setContractStartDate(model.getContractStartDate());
		soapModel.setContractEndDate(model.getContractEndDate());
		soapModel.setContractStatus(model.getContractStatus());
		soapModel.setApproverCategory(model.getApproverCategory());
		soapModel.setApprovalLevel(model.getApprovalLevel());
		soapModel.setAkiReasonCode(model.getAkiReasonCode());
		soapModel.setProcessId(model.getProcessId());
		soapModel.setFaLetter(model.isFaLetter());
		soapModel.setFbmLetter(model.isFbmLetter());
		soapModel.setCbmLetter(model.isCbmLetter());
		soapModel.setIraLicence(model.isIraLicence());
		soapModel.setFinalStatus(model.isFinalStatus());
		soapModel.setProcessedFlag(model.getProcessedFlag());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());

		return soapModel;
	}

	public static ComplianceSoap[] toSoapModels(Compliance[] models) {
		ComplianceSoap[] soapModels = new ComplianceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ComplianceSoap[][] toSoapModels(Compliance[][] models) {
		ComplianceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ComplianceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ComplianceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ComplianceSoap[] toSoapModels(List<Compliance> models) {
		List<ComplianceSoap> soapModels = new ArrayList<ComplianceSoap>(
			models.size());

		for (Compliance model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ComplianceSoap[soapModels.size()]);
	}

	public ComplianceSoap() {
	}

	public long getPrimaryKey() {
		return _complianceId;
	}

	public void setPrimaryKey(long pk) {
		setComplianceId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getComplianceId() {
		return _complianceId;
	}

	public void setComplianceId(long complianceId) {
		_complianceId = complianceId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getCreatedby() {
		return _createdby;
	}

	public void setCreatedby(long createdby) {
		_createdby = createdby;
	}

	public String getModifiedby() {
		return _modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		_modifiedby = modifiedby;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getContractId() {
		return _contractId;
	}

	public void setContractId(long contractId) {
		_contractId = contractId;
	}

	public long getEntityId() {
		return _entityId;
	}

	public void setEntityId(long entityId) {
		_entityId = entityId;
	}

	public Date getApprovalDate() {
		return _approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		_approvalDate = approvalDate;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	public String getApproverComments() {
		return _approverComments;
	}

	public void setApproverComments(String approverComments) {
		_approverComments = approverComments;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getMiddleName() {
		return _middleName;
	}

	public void setMiddleName(String middleName) {
		_middleName = middleName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public String getContractNumber() {
		return _contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		_contractNumber = contractNumber;
	}

	public String getApproverContractNumber() {
		return _approverContractNumber;
	}

	public void setApproverContractNumber(String approverContractNumber) {
		_approverContractNumber = approverContractNumber;
	}

	public String getApproverVerdict() {
		return _approverVerdict;
	}

	public void setApproverVerdict(String approverVerdict) {
		_approverVerdict = approverVerdict;
	}

	public String getApproverAction() {
		return _approverAction;
	}

	public void setApproverAction(String approverAction) {
		_approverAction = approverAction;
	}

	public String getErrorReason() {
		return _errorReason;
	}

	public void setErrorReason(String errorReason) {
		_errorReason = errorReason;
	}

	public String getAssignmentStatus() {
		return _assignmentStatus;
	}

	public void setAssignmentStatus(String assignmentStatus) {
		_assignmentStatus = assignmentStatus;
	}

	public String getCapacity() {
		return _capacity;
	}

	public void setCapacity(String capacity) {
		_capacity = capacity;
	}

	public String getNewPositionId1() {
		return _newPositionId1;
	}

	public void setNewPositionId1(String newPositionId1) {
		_newPositionId1 = newPositionId1;
	}

	public String getNewPositionName1() {
		return _newPositionName1;
	}

	public void setNewPositionName1(String newPositionName1) {
		_newPositionName1 = newPositionName1;
	}

	public String getNewPositionId2() {
		return _newPositionId2;
	}

	public void setNewPositionId2(String newPositionId2) {
		_newPositionId2 = newPositionId2;
	}

	public String getNewPositionName2() {
		return _newPositionName2;
	}

	public void setNewPositionName2(String newPositionName2) {
		_newPositionName2 = newPositionName2;
	}

	public String getNewOrgId1() {
		return _newOrgId1;
	}

	public void setNewOrgId1(String newOrgId1) {
		_newOrgId1 = newOrgId1;
	}

	public String getNewOrgName1() {
		return _newOrgName1;
	}

	public void setNewOrgName1(String newOrgName1) {
		_newOrgName1 = newOrgName1;
	}

	public String getNewOrgId2() {
		return _newOrgId2;
	}

	public void setNewOrgId2(String newOrgId2) {
		_newOrgId2 = newOrgId2;
	}

	public String getNewOrgName2() {
		return _newOrgName2;
	}

	public void setNewOrgName2(String newOrgName2) {
		_newOrgName2 = newOrgName2;
	}

	public String getHistoryStartDate() {
		return _historyStartDate;
	}

	public void setHistoryStartDate(String historyStartDate) {
		_historyStartDate = historyStartDate;
	}

	public String getHistoryEndDate() {
		return _historyEndDate;
	}

	public void setHistoryEndDate(String historyEndDate) {
		_historyEndDate = historyEndDate;
	}

	public String getOldOrgId() {
		return _oldOrgId;
	}

	public void setOldOrgId(String oldOrgId) {
		_oldOrgId = oldOrgId;
	}

	public String getOldPositionId() {
		return _oldPositionId;
	}

	public void setOldPositionId(String oldPositionId) {
		_oldPositionId = oldPositionId;
	}

	public String getContractStartDate() {
		return _contractStartDate;
	}

	public void setContractStartDate(String contractStartDate) {
		_contractStartDate = contractStartDate;
	}

	public String getContractEndDate() {
		return _contractEndDate;
	}

	public void setContractEndDate(String contractEndDate) {
		_contractEndDate = contractEndDate;
	}

	public String getContractStatus() {
		return _contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		_contractStatus = contractStatus;
	}

	public String getApproverCategory() {
		return _approverCategory;
	}

	public void setApproverCategory(String approverCategory) {
		_approverCategory = approverCategory;
	}

	public String getApprovalLevel() {
		return _approvalLevel;
	}

	public void setApprovalLevel(String approvalLevel) {
		_approvalLevel = approvalLevel;
	}

	public String getAkiReasonCode() {
		return _akiReasonCode;
	}

	public void setAkiReasonCode(String akiReasonCode) {
		_akiReasonCode = akiReasonCode;
	}

	public String getProcessId() {
		return _processId;
	}

	public void setProcessId(String processId) {
		_processId = processId;
	}

	public boolean getFaLetter() {
		return _faLetter;
	}

	public boolean isFaLetter() {
		return _faLetter;
	}

	public void setFaLetter(boolean faLetter) {
		_faLetter = faLetter;
	}

	public boolean getFbmLetter() {
		return _fbmLetter;
	}

	public boolean isFbmLetter() {
		return _fbmLetter;
	}

	public void setFbmLetter(boolean fbmLetter) {
		_fbmLetter = fbmLetter;
	}

	public boolean getCbmLetter() {
		return _cbmLetter;
	}

	public boolean isCbmLetter() {
		return _cbmLetter;
	}

	public void setCbmLetter(boolean cbmLetter) {
		_cbmLetter = cbmLetter;
	}

	public boolean getIraLicence() {
		return _iraLicence;
	}

	public boolean isIraLicence() {
		return _iraLicence;
	}

	public void setIraLicence(boolean iraLicence) {
		_iraLicence = iraLicence;
	}

	public boolean getFinalStatus() {
		return _finalStatus;
	}

	public boolean isFinalStatus() {
		return _finalStatus;
	}

	public void setFinalStatus(boolean finalStatus) {
		_finalStatus = finalStatus;
	}

	public String getProcessedFlag() {
		return _processedFlag;
	}

	public void setProcessedFlag(String processedFlag) {
		_processedFlag = processedFlag;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	private String _uuid;
	private long _complianceId;
	private long _groupId;
	private long _companyId;
	private long _createdby;
	private String _modifiedby;
	private Date _createDate;
	private Date _modifiedDate;
	private long _contractId;
	private long _entityId;
	private Date _approvalDate;
	private String _comments;
	private String _approverComments;
	private String _firstName;
	private String _middleName;
	private String _lastName;
	private String _contractNumber;
	private String _approverContractNumber;
	private String _approverVerdict;
	private String _approverAction;
	private String _errorReason;
	private String _assignmentStatus;
	private String _capacity;
	private String _newPositionId1;
	private String _newPositionName1;
	private String _newPositionId2;
	private String _newPositionName2;
	private String _newOrgId1;
	private String _newOrgName1;
	private String _newOrgId2;
	private String _newOrgName2;
	private String _historyStartDate;
	private String _historyEndDate;
	private String _oldOrgId;
	private String _oldPositionId;
	private String _contractStartDate;
	private String _contractEndDate;
	private String _contractStatus;
	private String _approverCategory;
	private String _approvalLevel;
	private String _akiReasonCode;
	private String _processId;
	private boolean _faLetter;
	private boolean _fbmLetter;
	private boolean _cbmLetter;
	private boolean _iraLicence;
	private boolean _finalStatus;
	private String _processedFlag;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;

}