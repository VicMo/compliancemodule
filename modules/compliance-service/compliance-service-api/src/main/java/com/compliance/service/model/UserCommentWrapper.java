/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserComment}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserComment
 * @generated
 */
public class UserCommentWrapper
	implements ModelWrapper<UserComment>, UserComment {

	public UserCommentWrapper(UserComment userComment) {
		_userComment = userComment;
	}

	@Override
	public Class<?> getModelClass() {
		return UserComment.class;
	}

	@Override
	public String getModelClassName() {
		return UserComment.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("commentId", getCommentId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createdby", getCreatedby());
		attributes.put("modifiedby", getModifiedby());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("commentDate", getCommentDate());
		attributes.put("approverName", getApproverName());
		attributes.put("approverContractNumber", getApproverContractNumber());
		attributes.put("comment", getComment());
		attributes.put("complianceId", getComplianceId());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long commentId = (Long)attributes.get("commentId");

		if (commentId != null) {
			setCommentId(commentId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long createdby = (Long)attributes.get("createdby");

		if (createdby != null) {
			setCreatedby(createdby);
		}

		String modifiedby = (String)attributes.get("modifiedby");

		if (modifiedby != null) {
			setModifiedby(modifiedby);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date commentDate = (Date)attributes.get("commentDate");

		if (commentDate != null) {
			setCommentDate(commentDate);
		}

		String approverName = (String)attributes.get("approverName");

		if (approverName != null) {
			setApproverName(approverName);
		}

		String approverContractNumber = (String)attributes.get(
			"approverContractNumber");

		if (approverContractNumber != null) {
			setApproverContractNumber(approverContractNumber);
		}

		String comment = (String)attributes.get("comment");

		if (comment != null) {
			setComment(comment);
		}

		Long complianceId = (Long)attributes.get("complianceId");

		if (complianceId != null) {
			setComplianceId(complianceId);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}
	}

	@Override
	public Object clone() {
		return new UserCommentWrapper((UserComment)_userComment.clone());
	}

	@Override
	public int compareTo(UserComment userComment) {
		return _userComment.compareTo(userComment);
	}

	/**
	 * Returns the approver contract number of this user comment.
	 *
	 * @return the approver contract number of this user comment
	 */
	@Override
	public String getApproverContractNumber() {
		return _userComment.getApproverContractNumber();
	}

	/**
	 * Returns the approver name of this user comment.
	 *
	 * @return the approver name of this user comment
	 */
	@Override
	public String getApproverName() {
		return _userComment.getApproverName();
	}

	/**
	 * Returns the comment of this user comment.
	 *
	 * @return the comment of this user comment
	 */
	@Override
	public String getComment() {
		return _userComment.getComment();
	}

	/**
	 * Returns the comment date of this user comment.
	 *
	 * @return the comment date of this user comment
	 */
	@Override
	public Date getCommentDate() {
		return _userComment.getCommentDate();
	}

	/**
	 * Returns the comment ID of this user comment.
	 *
	 * @return the comment ID of this user comment
	 */
	@Override
	public long getCommentId() {
		return _userComment.getCommentId();
	}

	/**
	 * Returns the company ID of this user comment.
	 *
	 * @return the company ID of this user comment
	 */
	@Override
	public long getCompanyId() {
		return _userComment.getCompanyId();
	}

	/**
	 * Returns the compliance ID of this user comment.
	 *
	 * @return the compliance ID of this user comment
	 */
	@Override
	public long getComplianceId() {
		return _userComment.getComplianceId();
	}

	/**
	 * Returns the create date of this user comment.
	 *
	 * @return the create date of this user comment
	 */
	@Override
	public Date getCreateDate() {
		return _userComment.getCreateDate();
	}

	/**
	 * Returns the createdby of this user comment.
	 *
	 * @return the createdby of this user comment
	 */
	@Override
	public long getCreatedby() {
		return _userComment.getCreatedby();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userComment.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this user comment.
	 *
	 * @return the group ID of this user comment
	 */
	@Override
	public long getGroupId() {
		return _userComment.getGroupId();
	}

	/**
	 * Returns the modifiedby of this user comment.
	 *
	 * @return the modifiedby of this user comment
	 */
	@Override
	public String getModifiedby() {
		return _userComment.getModifiedby();
	}

	/**
	 * Returns the modified date of this user comment.
	 *
	 * @return the modified date of this user comment
	 */
	@Override
	public Date getModifiedDate() {
		return _userComment.getModifiedDate();
	}

	/**
	 * Returns the primary key of this user comment.
	 *
	 * @return the primary key of this user comment
	 */
	@Override
	public long getPrimaryKey() {
		return _userComment.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userComment.getPrimaryKeyObj();
	}

	/**
	 * Returns the status of this user comment.
	 *
	 * @return the status of this user comment
	 */
	@Override
	public int getStatus() {
		return _userComment.getStatus();
	}

	/**
	 * Returns the status by user ID of this user comment.
	 *
	 * @return the status by user ID of this user comment
	 */
	@Override
	public long getStatusByUserId() {
		return _userComment.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this user comment.
	 *
	 * @return the status by user name of this user comment
	 */
	@Override
	public String getStatusByUserName() {
		return _userComment.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this user comment.
	 *
	 * @return the status by user uuid of this user comment
	 */
	@Override
	public String getStatusByUserUuid() {
		return _userComment.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this user comment.
	 *
	 * @return the status date of this user comment
	 */
	@Override
	public Date getStatusDate() {
		return _userComment.getStatusDate();
	}

	/**
	 * Returns the uuid of this user comment.
	 *
	 * @return the uuid of this user comment
	 */
	@Override
	public String getUuid() {
		return _userComment.getUuid();
	}

	@Override
	public int hashCode() {
		return _userComment.hashCode();
	}

	/**
	 * Returns <code>true</code> if this user comment is approved.
	 *
	 * @return <code>true</code> if this user comment is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return _userComment.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _userComment.isCachedModel();
	}

	/**
	 * Returns <code>true</code> if this user comment is denied.
	 *
	 * @return <code>true</code> if this user comment is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return _userComment.isDenied();
	}

	/**
	 * Returns <code>true</code> if this user comment is a draft.
	 *
	 * @return <code>true</code> if this user comment is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return _userComment.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _userComment.isEscapedModel();
	}

	/**
	 * Returns <code>true</code> if this user comment is expired.
	 *
	 * @return <code>true</code> if this user comment is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return _userComment.isExpired();
	}

	/**
	 * Returns <code>true</code> if this user comment is inactive.
	 *
	 * @return <code>true</code> if this user comment is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return _userComment.isInactive();
	}

	/**
	 * Returns <code>true</code> if this user comment is incomplete.
	 *
	 * @return <code>true</code> if this user comment is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return _userComment.isIncomplete();
	}

	@Override
	public boolean isNew() {
		return _userComment.isNew();
	}

	/**
	 * Returns <code>true</code> if this user comment is pending.
	 *
	 * @return <code>true</code> if this user comment is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return _userComment.isPending();
	}

	/**
	 * Returns <code>true</code> if this user comment is scheduled.
	 *
	 * @return <code>true</code> if this user comment is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return _userComment.isScheduled();
	}

	@Override
	public void persist() {
		_userComment.persist();
	}

	/**
	 * Sets the approver contract number of this user comment.
	 *
	 * @param approverContractNumber the approver contract number of this user comment
	 */
	@Override
	public void setApproverContractNumber(String approverContractNumber) {
		_userComment.setApproverContractNumber(approverContractNumber);
	}

	/**
	 * Sets the approver name of this user comment.
	 *
	 * @param approverName the approver name of this user comment
	 */
	@Override
	public void setApproverName(String approverName) {
		_userComment.setApproverName(approverName);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userComment.setCachedModel(cachedModel);
	}

	/**
	 * Sets the comment of this user comment.
	 *
	 * @param comment the comment of this user comment
	 */
	@Override
	public void setComment(String comment) {
		_userComment.setComment(comment);
	}

	/**
	 * Sets the comment date of this user comment.
	 *
	 * @param commentDate the comment date of this user comment
	 */
	@Override
	public void setCommentDate(Date commentDate) {
		_userComment.setCommentDate(commentDate);
	}

	/**
	 * Sets the comment ID of this user comment.
	 *
	 * @param commentId the comment ID of this user comment
	 */
	@Override
	public void setCommentId(long commentId) {
		_userComment.setCommentId(commentId);
	}

	/**
	 * Sets the company ID of this user comment.
	 *
	 * @param companyId the company ID of this user comment
	 */
	@Override
	public void setCompanyId(long companyId) {
		_userComment.setCompanyId(companyId);
	}

	/**
	 * Sets the compliance ID of this user comment.
	 *
	 * @param complianceId the compliance ID of this user comment
	 */
	@Override
	public void setComplianceId(long complianceId) {
		_userComment.setComplianceId(complianceId);
	}

	/**
	 * Sets the create date of this user comment.
	 *
	 * @param createDate the create date of this user comment
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_userComment.setCreateDate(createDate);
	}

	/**
	 * Sets the createdby of this user comment.
	 *
	 * @param createdby the createdby of this user comment
	 */
	@Override
	public void setCreatedby(long createdby) {
		_userComment.setCreatedby(createdby);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_userComment.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userComment.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userComment.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this user comment.
	 *
	 * @param groupId the group ID of this user comment
	 */
	@Override
	public void setGroupId(long groupId) {
		_userComment.setGroupId(groupId);
	}

	/**
	 * Sets the modifiedby of this user comment.
	 *
	 * @param modifiedby the modifiedby of this user comment
	 */
	@Override
	public void setModifiedby(String modifiedby) {
		_userComment.setModifiedby(modifiedby);
	}

	/**
	 * Sets the modified date of this user comment.
	 *
	 * @param modifiedDate the modified date of this user comment
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_userComment.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_userComment.setNew(n);
	}

	/**
	 * Sets the primary key of this user comment.
	 *
	 * @param primaryKey the primary key of this user comment
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_userComment.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userComment.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the status of this user comment.
	 *
	 * @param status the status of this user comment
	 */
	@Override
	public void setStatus(int status) {
		_userComment.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this user comment.
	 *
	 * @param statusByUserId the status by user ID of this user comment
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_userComment.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this user comment.
	 *
	 * @param statusByUserName the status by user name of this user comment
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		_userComment.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this user comment.
	 *
	 * @param statusByUserUuid the status by user uuid of this user comment
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_userComment.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this user comment.
	 *
	 * @param statusDate the status date of this user comment
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		_userComment.setStatusDate(statusDate);
	}

	/**
	 * Sets the uuid of this user comment.
	 *
	 * @param uuid the uuid of this user comment
	 */
	@Override
	public void setUuid(String uuid) {
		_userComment.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<UserComment>
		toCacheModel() {

		return _userComment.toCacheModel();
	}

	@Override
	public UserComment toEscapedModel() {
		return new UserCommentWrapper(_userComment.toEscapedModel());
	}

	@Override
	public String toString() {
		return _userComment.toString();
	}

	@Override
	public UserComment toUnescapedModel() {
		return new UserCommentWrapper(_userComment.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _userComment.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserCommentWrapper)) {
			return false;
		}

		UserCommentWrapper userCommentWrapper = (UserCommentWrapper)obj;

		if (Objects.equals(_userComment, userCommentWrapper._userComment)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _userComment.getStagedModelType();
	}

	@Override
	public UserComment getWrappedModel() {
		return _userComment;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userComment.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userComment.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userComment.resetOriginalValues();
	}

	private final UserComment _userComment;

}