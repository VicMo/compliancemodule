/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserCommentLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserCommentLocalService
 * @generated
 */
public class UserCommentLocalServiceWrapper
	implements ServiceWrapper<UserCommentLocalService>,
			   UserCommentLocalService {

	public UserCommentLocalServiceWrapper(
		UserCommentLocalService userCommentLocalService) {

		_userCommentLocalService = userCommentLocalService;
	}

	/**
	 * Adds the user comment to the database. Also notifies the appropriate model listeners.
	 *
	 * @param userComment the user comment
	 * @return the user comment that was added
	 */
	@Override
	public com.compliance.service.model.UserComment addUserComment(
		com.compliance.service.model.UserComment userComment) {

		return _userCommentLocalService.addUserComment(userComment);
	}

	/**
	 * Creates a new user comment with the primary key. Does not add the user comment to the database.
	 *
	 * @param commentId the primary key for the new user comment
	 * @return the new user comment
	 */
	@Override
	public com.compliance.service.model.UserComment createUserComment(
		long commentId) {

		return _userCommentLocalService.createUserComment(commentId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userCommentLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the user comment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment that was removed
	 * @throws PortalException if a user comment with the primary key could not be found
	 */
	@Override
	public com.compliance.service.model.UserComment deleteUserComment(
			long commentId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userCommentLocalService.deleteUserComment(commentId);
	}

	/**
	 * Deletes the user comment from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userComment the user comment
	 * @return the user comment that was removed
	 */
	@Override
	public com.compliance.service.model.UserComment deleteUserComment(
		com.compliance.service.model.UserComment userComment) {

		return _userCommentLocalService.deleteUserComment(userComment);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userCommentLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _userCommentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _userCommentLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _userCommentLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _userCommentLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _userCommentLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.compliance.service.model.UserComment fetchUserComment(
		long commentId) {

		return _userCommentLocalService.fetchUserComment(commentId);
	}

	/**
	 * Returns the user comment matching the UUID and group.
	 *
	 * @param uuid the user comment's UUID
	 * @param groupId the primary key of the group
	 * @return the matching user comment, or <code>null</code> if a matching user comment could not be found
	 */
	@Override
	public com.compliance.service.model.UserComment
		fetchUserCommentByUuidAndGroupId(String uuid, long groupId) {

		return _userCommentLocalService.fetchUserCommentByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns all the user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching user comments
	 */
	@Override
	public java.util.List<com.compliance.service.model.UserComment>
		findByComplianceId(long complianceId) {

		return _userCommentLocalService.findByComplianceId(complianceId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _userCommentLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _userCommentLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _userCommentLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _userCommentLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userCommentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the user comment with the primary key.
	 *
	 * @param commentId the primary key of the user comment
	 * @return the user comment
	 * @throws PortalException if a user comment with the primary key could not be found
	 */
	@Override
	public com.compliance.service.model.UserComment getUserComment(
			long commentId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userCommentLocalService.getUserComment(commentId);
	}

	/**
	 * Returns the user comment matching the UUID and group.
	 *
	 * @param uuid the user comment's UUID
	 * @param groupId the primary key of the group
	 * @return the matching user comment
	 * @throws PortalException if a matching user comment could not be found
	 */
	@Override
	public com.compliance.service.model.UserComment
			getUserCommentByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userCommentLocalService.getUserCommentByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the user comments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.compliance.service.model.impl.UserCommentModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @return the range of user comments
	 */
	@Override
	public java.util.List<com.compliance.service.model.UserComment>
		getUserComments(int start, int end) {

		return _userCommentLocalService.getUserComments(start, end);
	}

	/**
	 * Returns all the user comments matching the UUID and company.
	 *
	 * @param uuid the UUID of the user comments
	 * @param companyId the primary key of the company
	 * @return the matching user comments, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.compliance.service.model.UserComment>
		getUserCommentsByUuidAndCompanyId(String uuid, long companyId) {

		return _userCommentLocalService.getUserCommentsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of user comments matching the UUID and company.
	 *
	 * @param uuid the UUID of the user comments
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of user comments
	 * @param end the upper bound of the range of user comments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching user comments, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.compliance.service.model.UserComment>
		getUserCommentsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.compliance.service.model.UserComment> orderByComparator) {

		return _userCommentLocalService.getUserCommentsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of user comments.
	 *
	 * @return the number of user comments
	 */
	@Override
	public int getUserCommentsCount() {
		return _userCommentLocalService.getUserCommentsCount();
	}

	/**
	 * Updates the user comment in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param userComment the user comment
	 * @return the user comment that was updated
	 */
	@Override
	public com.compliance.service.model.UserComment updateUserComment(
		com.compliance.service.model.UserComment userComment) {

		return _userCommentLocalService.updateUserComment(userComment);
	}

	@Override
	public UserCommentLocalService getWrappedService() {
		return _userCommentLocalService;
	}

	@Override
	public void setWrappedService(
		UserCommentLocalService userCommentLocalService) {

		_userCommentLocalService = userCommentLocalService;
	}

	private UserCommentLocalService _userCommentLocalService;

}