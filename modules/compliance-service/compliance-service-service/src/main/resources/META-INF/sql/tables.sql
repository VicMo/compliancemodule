create table bic_Compliance (
	uuid_ VARCHAR(75) null,
	complianceId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	createdby LONG,
	modifiedby VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	contractId LONG,
	entityId LONG,
	approvalDate DATE null,
	comments VARCHAR(75) null,
	approverComments VARCHAR(75) null,
	firstName VARCHAR(75) null,
	middleName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	contractNumber VARCHAR(75) null,
	approverContractNumber VARCHAR(75) null,
	approverVerdict VARCHAR(75) null,
	approverAction VARCHAR(75) null,
	errorReason VARCHAR(75) null,
	assignmentStatus VARCHAR(75) null,
	capacity VARCHAR(75) null,
	newPositionId1 VARCHAR(75) null,
	newPositionName1 VARCHAR(75) null,
	newPositionId2 VARCHAR(75) null,
	newPositionName2 VARCHAR(75) null,
	newOrgId1 VARCHAR(75) null,
	newOrgName1 VARCHAR(75) null,
	newOrgId2 VARCHAR(75) null,
	newOrgName2 VARCHAR(75) null,
	historyStartDate VARCHAR(75) null,
	historyEndDate VARCHAR(75) null,
	oldOrgId VARCHAR(75) null,
	oldPositionId VARCHAR(75) null,
	contractStartDate VARCHAR(75) null,
	contractEndDate VARCHAR(75) null,
	contractStatus VARCHAR(75) null,
	approverCategory VARCHAR(75) null,
	approvalLevel VARCHAR(75) null,
	akiReasonCode VARCHAR(75) null,
	processId VARCHAR(75) null,
	faLetter BOOLEAN,
	fbmLetter BOOLEAN,
	cbmLetter BOOLEAN,
	iraLicence BOOLEAN,
	finalStatus BOOLEAN,
	processedFlag VARCHAR(75) null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);

create table bic_UserComment (
	uuid_ VARCHAR(75) null,
	commentId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	createdby LONG,
	modifiedby VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	commentDate DATE null,
	approverName VARCHAR(75) null,
	approverContractNumber VARCHAR(75) null,
	comment_ VARCHAR(75) null,
	complianceId LONG,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);