package com.compliance.holders;;

public class Approvals {
	
	long approvalId;
	String processName;
	String approvalPositionID;
	
	public long getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(long approvalId) {
		this.approvalId = approvalId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getApprovalPositionID() {
		return approvalPositionID;
	}
	public void setApprovalPositionID(String approvalPositionID) {
		this.approvalPositionID = approvalPositionID;
	}

}
