package com.compliance.holders;

public class ProcessFlow {
	long processFlowId;
	String processFlowName;
	String processCategory;
	String processId;

	public long getProcessFlowId() {
		return processFlowId;
	}

	public void setProcessFlowId(long processFlowId) {
		this.processFlowId = processFlowId;
	}

	public String getProcessFlowName() {
		return processFlowName;
	}

	public void setProcessFlowName(String processFlowName) {
		this.processFlowName = processFlowName;
	}

	public String getProcessCategory() {
		return processCategory;
	}

	public void setProcessCategory(String processCategory) {
		this.processCategory = processCategory;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

}
