package com.compliance.utils;

import com.compliance.holders.AKIReasons;
import com.compliance.holders.AssignmentStatus;
import com.compliance.holders.Intermediaries;
import com.compliance.holders.Organizations;
import com.compliance.holders.Positions;
import com.compliance.holders.ProcessFlow;
import com.compliance.service.model.Compliance;
import com.compliance.service.service.ComplianceLocalServiceUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComplianceUtils {

	/* creates in session current logged in user profile */
	public static Map<String, Object> createCurrentUserProfile(String contractNumber) {

		Map<String, Object> map = new HashMap<String, Object>();

		try {

			long userPositionID;
			long userOrgID;
			String userPositionName;

			StringBuffer responseContent = new StringBuffer();

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/fadetails/" + contractNumber);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				userPositionName = fa.getString("position_name");
				userOrgID = fa.getLong("organization_id");
				userPositionID = fa.getLong("position_id");

				map.put("user_position_name", fa.getString("position_name"));
				map.put("user_organization_id", fa.getLong("organization_id"));
				map.put("user_position_id", fa.getLong("position_id"));

				System.out.println(
						"_____________________util_selectedOrgId________: " + (long) map.get("user_organization_id"));
				System.out.println(
						"_____________________util_selectedPositionName_: " + (String) map.get("user_position_name"));

				break;
			}

		} catch (Exception e) {
			System.out.println("Exception :- ");
			e.printStackTrace();
		}
		return map;

	}

	/* returns list of processes assigned to given position */
	public static List<ProcessFlow> createProcessFlow(long userPositionID, long selectedIntermediaryPositionId) {
		// show data in view.
		List<ProcessFlow> process = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			System.out.println("______________userPositionId         : " + userPositionID);
			System.out.println("______________intermediaryPositionId : " + selectedIntermediaryPositionId);
			System.out.println("______________action                 : Request");

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getprocessnew/" + userPositionID
					+ "," + selectedIntermediaryPositionId + "," + "Request");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				ProcessFlow processFlow = new ProcessFlow();
				processFlow.setProcessFlowId(fa.getLong("compliance_approval_id"));
				processFlow.setProcessFlowName(fa.getString("process_name"));
				processFlow.setProcessCategory(fa.getString("process_category"));
				processFlow.setProcessId(fa.getString("process_id"));

				process.add(processFlow);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return process;
	}

	/* returns list of processes assigned to given position */
	public static List<ProcessFlow> createProcessFlow(long userPositionID) {
		// show data in view.
		List<ProcessFlow> process = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			System.out.println("______________userPositionId         : " + userPositionID);
			System.out.println("______________action                 : Approval");

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getprocesstype/" + userPositionID
					+ ",Approval");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				ProcessFlow processFlow = new ProcessFlow();
				processFlow.setProcessCategory(fa.getString("process_category"));

				process.add(processFlow);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return process;
	}

	/* returns returnd filtered list based on supplied list of processIds */
	public static List<Compliance> filterProcesList(List<Compliance> rawComplianceList, long userPositionID) {

		System.out.println("______________++++++++++++++++++++++++++++ start filter         : ");

		// retrieve applicable processIds for signed in user
		List<ProcessFlow> approvalProcesses = new ArrayList<>();
		approvalProcesses = createProcessFlow(userPositionID);

		System.out.println("______________++userPositionID : " + userPositionID);
		System.out.println("______________++size of approvalProcesses: " + approvalProcesses.size());
		System.out.println("______________++size of rawComplianceList: " + rawComplianceList.size());

		List<Compliance> complianceList = new ArrayList<>();

		// filter the list to show only those entries that match the criteria
		if (rawComplianceList != null) {
			for (Compliance compliance : rawComplianceList) {

				for (ProcessFlow processs : approvalProcesses) {
					System.out.println("______________++compliance.getApproverCategory() : " + compliance.getApproverCategory());
					System.out.println("______________++(processs.getProcessCategory() : " + processs.getProcessCategory());
					
					if (compliance.getApproverCategory().equalsIgnoreCase(processs.getProcessCategory())) {
						complianceList.add(compliance);
					}

				}
			}
		}

		System.out.println("______________++size of FINAL complianceList: " + complianceList.size());
		return complianceList;
	}

	/* returns list of processes assigned to given position */
	public static List<AKIReasons> createAKIReasons() {
		// show data in view.
		List<AKIReasons> aKIReasonsList = new ArrayList<>();
		try {
			StringBuffer responseContent = new StringBuffer();

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/akireasons/");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				AKIReasons aKIReasons = new AKIReasons();
				aKIReasons.setReasonCode(fa.getString("aki_reason_code"));
				aKIReasons.setReasonName(fa.getString("aki_reason_name"));

				aKIReasonsList.add(aKIReasons);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return aKIReasonsList;
	}

	/* returns list of organizations */
	public static List<Organizations> createOrganizations(String urlString, int orgTypeId) {
		// show data in view.
		List<Organizations> organizationsList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			URL url = new URL(urlString + orgTypeId);

			// URL url = new
			// URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/orgs/");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				Organizations organizations = new Organizations();
				organizations.setOrgId(fa.getLong("org_id"));
				organizations.setOrgName(fa.getString("org_name"));

				organizationsList.add(organizations);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();

		}

		return organizationsList;
	}

	/* 28th May:- Created the new List for Child Organizations */
	public static List<Organizations> createChildOrganizations(long parentVal) {
		// show data in view.
		List<Organizations> childOrganizationsList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();
			// String portletType = ParamUtil.getString(ActionRequest, "portletType");

			// int parentVal = 352;
			/* ours */URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + parentVal);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				Organizations organizations = new Organizations();
				organizations.setOrgId(fa.getLong("org_id"));
				organizations.setOrgName(fa.getString("org_name"));

				childOrganizationsList.add(organizations);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return childOrganizationsList;
	}

	/* returns list of assignment status */
	public static List<AssignmentStatus> createAssignmentStatus() {
		// show data in view.
		List<AssignmentStatus> assignmentStatusList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/assignmentStatus/");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				AssignmentStatus assignmentStatus = new AssignmentStatus();
				assignmentStatus.setStatusId(fa.getLong("assignment_status_id"));
				assignmentStatus.setStatusName(fa.getString("assignment_status_name"));

				assignmentStatusList.add(assignmentStatus);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return assignmentStatusList;
	}

	/* returns list of positions */
	public static List<Positions> createPositions() {
		// show data in view.
		List<Positions> positionsList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/positions/");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				Positions positions = new Positions();
				positions.setPositionId(fa.getLong("position_id"));
				positions.setPositionName(fa.getString("position_name"));

				positionsList.add(positions);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return positionsList;
	}

	/* returns list of organizations */
	public static List<Organizations> createOrganizations(long parentId) {
		// show data in view.
		List<Organizations> organizationsList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();

			URL url = new URL("http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getorgs/" + parentId);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");
			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				Organizations organizations = new Organizations();
				organizations.setOrgId(fa.getLong("org_id"));
				organizations.setOrgName(fa.getString("org_name"));

				organizationsList.add(organizations);
			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return organizationsList;
	}

	/* returns intermediaries List from web service */
	public static List<Intermediaries> createIntermediaries(long drillDownOrgId, String hierachyPosName) {
		List<Intermediaries> intermediaryList = new ArrayList<>();
		try {

			StringBuffer responseContent = new StringBuffer();
			URL url = new URL(determineServiceToCall(hierachyPosName, drillDownOrgId));

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {

				responseContent.append(output);
			}
			br.close();
			conn.disconnect();

			// call method to create objects
			JSONObject jObj = JSONFactoryUtil.createJSONObject(responseContent.toString());
			JSONArray fas = jObj.getJSONArray("items");

			for (int i = 0; i < fas.length(); i++) {
				JSONObject fa = fas.getJSONObject(i);

				Intermediaries intermediary = new Intermediaries();
				intermediary.setContractNumber(fa.getString("contract_number"));
				intermediary.setFirstName(fa.getString("first_name"));
				intermediary.setMiddleName(fa.getString("middle_name"));
				intermediary.setLastName(fa.getString("last_name"));
				intermediary.setOrgId(fa.getLong("org_id"));
				intermediary.setOrgName(fa.getString("org_name"));
				intermediary.setPositionName(fa.getString("position_name"));
				intermediary.setEntityId(fa.getLong("entity_id"));
				intermediary.setContractId(fa.getLong("contract_id"));
				intermediary.setPositionId(fa.getLong("position_id"));
				intermediary.setCapacity(fa.getLong("capacity"));
				intermediary.setAssignmentStatus(fa.getString("assignment_status"));
				intermediary.setContractESDate(fa.getString(""));
				intermediary.setContractESDate(fa.getString(""));
				intermediary.setHistoryESDate(fa.getString(""));
				intermediary.setHistoryEEDate(fa.getString(""));

				intermediaryList.add(intermediary);

			}

		} catch (Exception e) {
			System.out.println("Exception :- " + e);
			e.printStackTrace();
		}

		return intermediaryList;
	}

	/*
	 * determine web service to be invoked getfasdetails or gethierarcy based on
	 * position
	 */
	public static String determineServiceToCall(String positionName, long drillDownOrgId) {
		String posName = positionName.toUpperCase();
		if (posName.equals("FINANCIAL ADVISOR") || posName.equals("UNIT MANAGER")) {

			return "http://10.10.4.214:9001/ords/apex_ebs_extension/fams/getfasdetails/" + drillDownOrgId;
		} else {
			return "http://10.10.4.214:9001/ords/apex_ebs_extension/fams/gethierarchy/" + drillDownOrgId;
		}
	}
}
